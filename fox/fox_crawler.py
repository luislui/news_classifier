import requests
import re
from selenium import webdriver # 从selenium导入webdriver
import time
from dateutil.parser import parse as time_parse
from selenium import webdriver # 从selenium导入webdriver
import csv
from dateutil.parser import parse as time_parse

class MSNBCCrawler():
    def __init__(self):
        self.search_page = "http://www.msnbc.com/search/hurricane%20Maria"
        self.result = []
        self.max_page = None
        self.url_and_title = []
        options = webdriver.ChromeOptions()
        options.add_argument('lang=zh_CN.UTF-8')

        options.add_argument(
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36')
        self.driver = webdriver.Chrome("../msnbc/chromedriver",
                                       chrome_options=options)  # Optional argument, if not specified will search path.
        self.driver.set_window_size(500, 800)

    def get_search_result_page(self):
        base_url = "https://www.foxnews.com/search-results/search?q=hurricane%20Maria"
        self.driver.get(base_url)

        for i in range(0,3000 ):
            try:
                show_more_btn = self.driver.find_element_by_css_selector('.load-more a')
                show_more_btn.click()
                time.sleep(1)
            except:
                continue

        time.sleep(5)
        search_list = self.driver.find_elements_by_css_selector('.collection-search .article')
        for i in search_list:
            title = i.text.split('\n')
            if len(title) > 1:
                title = title[1]
                url = i.find_element_by_css_selector('a').get_attribute('href')
                self.url_and_title.append({"url":url,"title":title})

    def get_content(self):
            self.driver.set_window_size(2000, 4001)
            for index,i in enumerate(self.url_and_title):
                try:
                    news_url = i["url"]
                    self.driver.get(news_url)
                    time.sleep(5)
                    if 'https://video.foxnews.com/v/' in news_url:
                        data = self.driver.find_elements_by_xpath('/html/body/div[2]/header/div/div/div/div[2]/p')[0].text.split('\n')[0]
                        date_posted = time_parse(data.split('-')[0].strip().replace('.',''))
                        i['date_posted'] = date_posted
                        content_ = data.split('-')[-1]
                        i['content'] = content_
                    else:
                        date_posted = time_parse(self.driver.find_element_by_css_selector('.article-date time').text)
                        i['date_posted'] = date_posted
                        if self.driver.page_source:
                            pass
                        content = self.driver.find_elements_by_css_selector('.article-body p')
                        i['content'] = ''
                        for j in content:
                            content_ = j.text
                            if 'The Associated Press contributed to this report.' not in content and \
                                    'hweitering@space.com' not in content and \
                                    'CLICK HERE FOR THE ALL-NEW ' not in content and \
                                    'contributed to this report' not in content and \
                                    '___' not in content :
                                #print(content_)
                                i['content'] += content_
                        #print(date_posted)
                except Exception as e:
                    print(e)
                    print(i)

    def insert_to_csv(self):
        col = ['title','post_dated','url','content']
        csvFile = open("fox.csv", "w")
        writer = csv.writer(csvFile)
        writer.writerow(col)
        for i in self.url_and_title:
            if 'content' in i.keys() and 'date_posted' in i.keys():
                # if 'puerto rico' in i['content'].lower() and 'hurricane maria' in i['content'].lower():
                i['date_posted'] = str(i['date_posted']).split(' ')[0]
                i['content'] = i['content'].replace('\n', '').strip()
                writer.writerow([i['title'], i['date_posted'], i['url'], i['content']])
            else:
                writer.writerow([i['title'], "", i['url'], ""])


if __name__ == '__main__':
    instance = MSNBCCrawler()
    instance.get_search_result_page()
    instance.get_content()
    instance.insert_to_csv()
    #
    # r = requests.get("http://www.msnbc.com/katy-tur/watch/hud-inspector-gen-investigating-if-wh-slowed-puerto-rico-aid-1466076227973")
    #
    # condition_of_video_parse = re.compile('bcnews.com/j/MSNBC/Components/Video/', flags=re.DOTALL)
    # # print(condition_of_video_parse)
    # condition_of_video_parse = re.findall(condition_of_video_parse, r.text)
    # if condition_of_video_parse:
    #     content_parse = re.compile('property="og:description" content="(.*?)"/>', flags=re.DOTALL)
    #     content = re.findall(content_parse, r.text)
    # else:
    #     r = requests.get(url)
    #     parse = re.compile('rticleBody">(.*?) </div>', flags=re.DOTALL)
    #     # print(r.text.replace("\n",""))
    #     find_page_result = re.findall(parse, r.text.replace("\n", ""))[0]
    #     filter_content = re.sub(r'<a.*?">', "", find_page_result).replace("</a>", "").replace("<p>", "").replace(
    #         "</p>", "")
    #     print(filter_content)
    # print()
    #
    # # print(r.text.replace("\n",""))
