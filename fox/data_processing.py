import csv
import sys

csv.field_size_limit(sys.maxsize)
csvFile = open("fox.csv", "r")
processed_csvFile = open("processed_fox.csv", "w")
reader = csv.reader(csvFile)
writer = csv.writer(processed_csvFile)
for index , item in enumerate(reader):

    if index == 1:
        writer.writerow(item)
        continue

    # filter the empty post_dated
    if item[1]:
        writer.writerow(item)
