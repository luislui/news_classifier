import requests
import re
from selenium import webdriver # 从selenium导入webdriver
import csv
from dateutil.parser import parse as time_parse
from selenium import webdriver # 从selenium导入webdriver

from dateutil.parser import parse as time_parse

class MSNBCCrawler():
    def __init__(self):
        self.search_page = "http://www.msnbc.com/search/hurricane%20Maria"
        self.result = []
        self.max_page = None
        self.url_and_title = []
        options = webdriver.ChromeOptions()
        # 设置中文
        options.add_argument('lang=zh_CN.UTF-8')
        # 更换头部
        options.add_argument(
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36')
        self.driver = webdriver.Chrome("../msnbc/chromedriver",
                                       chrome_options=options)  # Optional argument, if not specified will search path.

    def get_search_result_page(self):
        base_url = "https://www.npr.org/search?query=hurricane%20Maria&page={}"

        import time
        finish = '0 results found '
        page = 1
        while 1:
            self.driver.get(base_url.format(page))
            time.sleep(3)

            if finish in self.driver.page_source:
                break

            url = self.driver.find_elements_by_css_selector('.ais-InfiniteHits-item .title a')
            for i in url:
                url = i.get_attribute('href')

                title = url.split("/")[-1]
                self.url_and_title.append({
                    "url": url,
                    "title": title
                })

            page += 1


    def get_content(self):

            for index,i in enumerate(self.url_and_title):
                news_url = i["url"]
                try:
                    if 'npr' in news_url:
                        self.driver.get(news_url)
                        source = self.driver.page_source.replace("\n", "")
                        if "transcript storytext" in source:

                            for _ in range(0,3):
                                i["content"] = ""
                                url = self.driver.find_elements_by_css_selector('.transcript  p')

                                for j in url:
                                    try:
                                        i["content"] += j.text
                                    except:
                                        continue

                                if 'content' not in i or not i['content']:
                                    print()
                                else:
                                    break
                        else:
                            for _ in range(0,3):
                                i["content"] = ""
                                url = self.driver.find_elements_by_css_selector('#storytext p')

                                for j in url:
                                    try:
                                        i["content"] += j.text
                                    except:
                                        continue

                                if len(i['content']) == 0:
                                    print()
                                else:
                                    break

                    date_posted_parse = re.compile('<meta name="date" content="(.*?)">|<span class="date">(.*?)</span>', re.S)
                    date_posted = re.findall(date_posted_parse, source)[0]
                    i["date_posted"] = time_parse(date_posted[0])
                except Exception as e:
                    print(news_url)
                    print(e)
                    continue
    def insert_to_csv(self):
        col = ['title','post_dated','url','content']

        csvFile = open("npr.csv", "w")
        writer = csv.writer(csvFile)
        writer.writerow(col)
        for i in self.url_and_title:
            if 'content' in i.keys() and 'date_posted' in i.keys():
                # if 'puerto rico' in i['content'].lower() and 'hurricane maria' in i['content'].lower():
                i['date_posted'] = str(i['date_posted']).split(' ')[0]
                i['content'] = i['content'].replace('\n', '').strip()
                writer.writerow([i['title'], i['date_posted'], i['url'], i['content']])
            else:
                writer.writerow([i['title'], "", i['url'], ""])

    # def insert_to_csv(self):
    #             col = ['title', 'date_posted', 'url', 'content']
    #
    #             csvFile = open("npr.csv", "w")
    #             writer = csv.writer(csvFile)
    #             writer.writerow(col)
    #             for i in self.url_and_title:
    #                 if 'content' in i.keys():
    #                     if 'puerto rico' in i['content'].lower() and 'hurricane maria' in i['content'].lower():
    #                         writer.writerow([i['title'], i['date_posted'], i['url'], i['content']])


if __name__ == '__main__':
    instance = MSNBCCrawler()
    instance.get_search_result_page()
    instance.get_content()
    instance.insert_to_csv()

    #
    # r = requests.get("http://www.msnbc.com/katy-tur/watch/hud-inspector-gen-investigating-if-wh-slowed-puerto-rico-aid-1466076227973")
    #
    # condition_of_video_parse = re.compile('bcnews.com/j/MSNBC/Components/Video/', flags=re.DOTALL)
    # # print(condition_of_video_parse)
    # condition_of_video_parse = re.findall(condition_of_video_parse, r.text)
    # if condition_of_video_parse:
    #     content_parse = re.compile('property="og:description" content="(.*?)"/>', flags=re.DOTALL)
    #     content = re.findall(content_parse, r.text)
    # else:
    #     r = requests.get(url)
    #     parse = re.compile('rticleBody">(.*?) </div>', flags=re.DOTALL)
    #     # print(r.text.replace("\n",""))
    #     find_page_result = re.findall(parse, r.text.replace("\n", ""))[0]
    #     filter_content = re.sub(r'<a.*?">', "", find_page_result).replace("</a>", "").replace("<p>", "").replace(
    #         "</p>", "")
    #     print(filter_content)
    # print()
    #
    # # print(r.text.replace("\n",""))
