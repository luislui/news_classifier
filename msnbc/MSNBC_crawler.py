import requests
import re
from selenium import webdriver # 从selenium导入webdriver

from dateutil.parser import parse as time_parse
import csv

class MSNBCCrawler():
    def __init__(self):
        self.search_page = "http://www.msnbc.com/search/hurricane%20Maria"
        self.result = []
        self.max_page = None
        self.url_and_title = []
    def get_search_result_page(self):
        base_url = "http://www.msnbc.com/search/hurricane%20Maria?sm_field_issues=&sm_field_show=&date%5Bmin%5D&date%5Bmax%5D&date%5Bdate_selector%5D="
        r = requests.get(base_url)
        parse = re.compile('<a class="search-result__teaser__title__link" href="(.*?)">(.*?)</a', re.S)
        source = r.text
        date_time_parse = re.compile('class="article-date-posted">(.*?)</time>', re.S)
        paging_base = "http://www.msnbc.com/search/hurricane%20Maria?sm_field_issues=&sm_field_show=&date%5Bmin%5D&date%5Bmax%5D&date%5Bdate_selector%5D=&page={}"
        find_page = re.compile('pager__item"><a title=".*?">(.*?)</a><', re.S)
        find_page_result = re.findall(find_page, source)
        the_last_page = int(find_page_result[-1])
        page = 1
        while page - 1 < the_last_page:
            r = requests.get(paging_base.format(page))
            source = r.text
            re_result = re.findall(parse, source)
            for i in re_result:
                url = "http://www.msnbc.com/" + i[0]
                print(url)
                title = i[1]
                self.url_and_title.append({"url": url,
                                      "title": title})
            page += 1
    def get_content(self):
        for i in self.url_and_title:
            title = i["title"]
            url = i["url"]
            r = requests.get(url)
            condition_of_video_parse = re.compile('bcnews.com/j/MSNBC/Components/Video/', flags=re.DOTALL)
            # print(condition_of_video_parse)
            condition_of_video_parse = re.findall(condition_of_video_parse, r.text)
            if condition_of_video_parse:
                date_time_parse = re.compile('class="byline___3V_nz">(.*?)</span>', re.S)
                date_posted = re.findall(date_time_parse, r.text)[0]
                content_parse = re.compile('property="og:description" content="(.*?)"/>', flags=re.DOTALL)
                filter_content = re.findall(content_parse, r.text)[0].strip()
            else:
                date_time_parse = re.compile('itemprop="dateCreated" content="(.*?)">', re.S)
                date_posted = re.findall(date_time_parse, r.text)[0]
                parse = re.compile('rticleBody">(.*?) </div>', flags=re.DOTALL)
                # print(r.text.replace("\n",""))
                find_page_result = re.findall(parse, r.text.replace("\n", ""))[0]
                filter_content = re.sub(r'<a.*?">', "", find_page_result).replace("</a>", "").replace("<p>", "").replace(
                    "</p>", "").strip()
            i["content"] = filter_content
            print(date_posted)
            i["date_posted"] = time_parse(date_posted)
            print(i)
    def insert_to_csv(self):
        col = ['title','post_dated','url','content']

        csvFile = open("msnbc.csv", "w")
        writer = csv.writer(csvFile)
        writer.writerow(col)
        for i in self.url_and_title:
            if 'content' in i.keys() and 'date_posted' in i.keys():
                # if 'puerto rico' in i['content'].lower() and 'hurricane maria' in i['content'].lower():
                i['date_posted'] = str(i['date_posted']).split(' ')[0]
                i['content'] = i['content'].replace('\n', '').strip()
                writer.writerow([i['title'], i['date_posted'], i['url'], i['content']])
            else:
                writer.writerow([i['title'], "", i['url'], ""])


if __name__ == '__main__':
    instance = MSNBCCrawler()
    instance.get_search_result_page()
    instance.get_content()
    instance.insert_to_csv()
    #
    # r = requests.get("http://www.msnbc.com/katy-tur/watch/hud-inspector-gen-investigating-if-wh-slowed-puerto-rico-aid-1466076227973")
    #
    # condition_of_video_parse = re.compile('bcnews.com/j/MSNBC/Components/Video/', flags=re.DOTALL)
    # # print(condition_of_video_parse)
    # condition_of_video_parse = re.findall(condition_of_video_parse, r.text)
    # if condition_of_video_parse:
    #     content_parse = re.compile('property="og:description" content="(.*?)"/>', flags=re.DOTALL)
    #     content = re.findall(content_parse, r.text)
    # else:
    #     r = requests.get(url)
    #     parse = re.compile('rticleBody">(.*?) </div>', flags=re.DOTALL)
    #     # print(r.text.replace("\n",""))
    #     find_page_result = re.findall(parse, r.text.replace("\n", ""))[0]
    #     filter_content = re.sub(r'<a.*?">', "", find_page_result).replace("</a>", "").replace("<p>", "").replace(
    #         "</p>", "")
    #     print(filter_content)
    # print()
    #
    # # print(r.text.replace("\n",""))
