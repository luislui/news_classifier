import pandas as pd
from datetime import datetime as dt
from bokeh.io import output_notebook, show
from bokeh.models import HoverTool, DatetimeTickFormatter, NumeralTickFormatter, ColumnDataSource
from bokeh.plotting import figure
from dateutil.parser import parse as time_parse
from datetime import datetime, timedelta
sample=pd.DataFrame(pd.read_csv('processed_msnbc.csv'))
sample['post_dated'] = pd.to_datetime(sample['post_dated'])
for index, row in sample.iterrows():
   if  row['post_dated'] < datetime(2017, 9, 1):
        sample = sample.drop(index)
sample['count'] = 1
pre = sample.post_dated.dt.to_period("M")
_ = sample.groupby(pre)['count'].sum()
count = _.array
x_axis = _.index.array

post_dated = []

for i in  _.index.array:
    post_dated.append(time_parse(str(i)))
count = []
for i in _.array:
    count.append(i)
chart_data = {
    'count': count, #this is the problem data
    'post_dated': post_dated,
}
source = ColumnDataSource(chart_data)
hover = HoverTool(
    tooltips=[
        ('count', '@count'),
        ('post_dated', '@post_dated{%Y - %B}'),
    ],
    formatters={
        'post_dated' : 'datetime',
    }
)

p = figure( x_axis_type='datetime',plot_width=600, plot_height=400, title='msnbc')
p.line(x='post_dated',y='count',source = source,     # 设置x，y值, source → 数据源
       line_width=1, line_alpha = 0.8, line_color = 'black',line_dash = [10,4])   # 线型基本设置
# 绘制折线图
p.circle(x='post_dated', y='count' ,source = source,
         size = 2,color = 'red',alpha = 0.8)
# 绘制折点

show(p)


# bar = figure(
#              x_axis_type='datetime',
#              plot_height=750,
#              plot_width=1000,
#              tools='wheel_zoom, reset, save',
#              title='ny times')


# bar.vbar(x='post_dated', top='count', width=0.9, source=source)
# bar.add_tools(hover)
#
# #visual settings
#
# bar.xaxis.formatter = DatetimeTickFormatter(years = ['%Y'])
# bar.xaxis.major_label_orientation = 'vertical'
# bar.xaxis.minor_tick_line_color = None
# bar.xgrid.grid_line_color = None
#
# bar.yaxis.formatter = NumeralTickFormatter()
# bar.ygrid.grid_line_color = None
#
# show(bar)
"""
chart_data = {
    'date': [a[0] for a in data['data']], #this is the problem data
    'gdp': [a[1] for a in data['data']],
}

source = ColumnDataSource(chart_data)
hover = HoverTool(
    tooltips=[
        ('GDP', '@gdp{$0,0.00}'),
        ('Date', '@date{%Y - %B}'),
    ],
    formatters={
        'date' : 'datetime',
    }
)

bar = figure(x_range=chart_data['date'],
             x_axis_type='datetime',
             plot_height=750,
             plot_width=1000,
             tools='wheel_zoom, reset, save',
             title=data['source_name'])
bar.vbar(x='date', top='gdp', width=0.9, source=source)
bar.add_tools(hover)

#visual settings

bar.xaxis.formatter = DatetimeTickFormatter(years = ['%Y'])
bar.xaxis.major_label_orientation = 'vertical'
bar.xaxis.minor_tick_line_color = None
bar.xgrid.grid_line_color = None

bar.yaxis.formatter = NumeralTickFormatter(format='$0,0.00')
bar.ygrid.grid_line_color = None

show(bar)

"""