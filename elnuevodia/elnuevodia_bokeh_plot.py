import pandas as pd
from datetime import datetime as dt
from bokeh.io import output_notebook, show
from bokeh.models import HoverTool, DatetimeTickFormatter, NumeralTickFormatter, ColumnDataSource
from bokeh.plotting import figure
from dateutil.parser import parse as time_parse
from datetime import datetime, timedelta
sample=pd.DataFrame(pd.read_csv('processed_elnuevodia.csv'))
sample['post_dated'] = pd.to_datetime(sample['post_dated'])
for index, row in sample.iterrows():
   if  row['post_dated'] < datetime(2017, 9, 1):
        sample = sample.drop(index)
sample['count'] = 1
pre = sample.post_dated.dt.to_period("M")
_ = sample.groupby(pre)['count'].sum()
count = _.array
x_axis = _.index.array

post_dated = []

for i in  _.index.array:
    post_dated.append(time_parse(str(i)))
count = []
for i in _.array:
    count.append(i)
chart_data = {
    'count': count,
    'post_dated': post_dated,
}
source = ColumnDataSource(chart_data)
hover = HoverTool(
    tooltips=[
        ('count', '@count'),
        ('post_dated', '@post_dated{%Y - %B}'),
    ],
    formatters={
        'post_dated' : 'datetime',
    }
)

p = figure( x_axis_type='datetime',plot_width=600, plot_height=400, title='elnuevodia')
p.line(x='post_dated',y='count',source = source,
       line_width=1, line_alpha = 0.8, line_color = 'black',line_dash = [10,4])

p.circle(x='post_dated', y='count' ,source = source,
         size = 2,color = 'red',alpha = 0.8)


show(p)

