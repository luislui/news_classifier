import pandas as pd
import copy
news_pd = pd.read_csv('./../processed_elnuevodia.csv',header=0)


import nltk
from sklearn.feature_extraction import text

eng_contractions = ["ain't", "amn't", "aren't", "can't", "could've", "couldn't",
                    "daresn't", "didn't", "doesn't", "don't", "gonna", "gotta",
                    "hadn't", "hasn't", "haven't", "he'd", "he'll", "he's", "how'd",
                    "how'll", "how's", "I'd", "I'll", "I'm", "I've", "isn't", "it'd",
                    "it'll", "it's", "let's", "mayn't", "may've", "mightn't",
                    "might've", "mustn't", "must've", "needn't", "o'clock", "ol'",
                    "oughtn't", "shan't", "she'd", "she'll", "she's", "should've",
                    "shouldn't", "somebody's", "someone's", "something's", "that'll",
                    "that're", "that's", "that'd", "there'd", "there're", "there's",
                    "these're", "they'd", "they'll", "they're", "they've", "this's",
                    "those're", "tis", "twas", "twasn't", "wasn't", "we'd", "we'd've",
                    "we'll", "we're", "we've", "weren't", "what'd", "what'll",
                    "what're", "what's", "what've", "when's", "where'd", "where're",
                    "where's", "where've", "which's", "who'd", "who'd've", "who'll",
                    "who're", "who's", "who've", "why'd", "why're", "why's", "won't",
                    "would've", "wouldn't", "y'all", "you'd", "you'll", "you're",
                    "you've", "'s", "s"
                     ]

nltk.download('stopwords')
nltk.download('punkt')
custom_stopwords = text.ENGLISH_STOP_WORDS.union(eng_contractions)
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")
import nltk
import re


def tokenize_and_stem(text, do_stem=True):
    # first tokenize by sentence, then by word to ensure that punctuation is caught as it's own token
    from nltk import word_tokenize, pos_tag
    nouns = []
    for token, pos in pos_tag(word_tokenize(text)):
        if pos.startswith('N') or pos.startswith('J'):
            nouns.append(token)
    tokens = [word.lower() for sent in nouns for word in nltk.word_tokenize(sent)]

    # filter out any tokens not containing letters (e.g., numeric tokens, raw punctuation)
    filtered_tokens = []

    for token in tokens:
        if re.search('[a-zA-Z]', token) and len(token)>3 and 'video' not in token and 'scrolling' not in token and 'onload' not in token  and '=' not in token   and '-' not in token  and '/' not in token :
            filtered_tokens.append(token)

    # stem filtered tokens
    stems = [stemmer.stem(t) for t in filtered_tokens]

    if do_stem:
        return stems
    else:
        return filtered_tokens


# not super pythonic, no, not at all.
# use extend so it's a big flat list of vocab
totalvocab_stemmed = []
totalvocab_tokenized = []
import numpy
content_list = []
loop_df = news_pd[pd.notnull(news_pd['content'])]
loop_df.drop_duplicates(subset='title',keep='first',inplace=True)
for i in loop_df['content'] :
    content_list.append(i)
    try:
        allwords_stemmed = tokenize_and_stem(i)
        totalvocab_stemmed.extend(allwords_stemmed)

        allwords_tokenized = tokenize_and_stem(i, False)
        totalvocab_tokenized.extend(allwords_tokenized)
    except:
        continue
vocab_frame = pd.DataFrame({'words': totalvocab_tokenized}, index = totalvocab_stemmed)
vocab_frame.head()

from sklearn.feature_extraction.text import TfidfVectorizer

#define vectorizer parameters
tfidf_vectorizer = TfidfVectorizer(
                                 min_df=0.01, stop_words=custom_stopwords,
                                 use_idf=True, tokenizer=tokenize_and_stem, ngram_range=(1,3))
import  numpy as np

tfidf_matrix = tfidf_vectorizer.fit_transform(loop_df['content']) #fit the vectorizer to synopses

terms = tfidf_vectorizer.get_feature_names()
_ = sorted(zip(terms, tfidf_vectorizer.idf_), key=lambda x: x[1])
for i in _:
    print(i)



terms = tfidf_vectorizer.get_feature_names()

from sklearn.cluster import KMeans
import math


km = KMeans(n_clusters=6)

km.fit(tfidf_matrix)

clusters = km.labels_.tolist()

loop_df['cluster'] = np.array(clusters)
loop_df.head()

print("Top terms per cluster:")
print()

# sort cluster centers by proximity to centroid
order_centroids = km.cluster_centers_.argsort()[:, ::-1]

import csv
import re
processed_csvFile = open("classifier.csv", "w", encoding='iso-8859-1', newline ='')

writer = csv.writer(processed_csvFile)
all_data = np.array(loop_df)
all_data = all_data.tolist()
first_type = []
sec_type = []
third_type = []
four_type = []
five_type = []
six_type = []
for i , index in zip(all_data,clusters):
    switch = {
        0 : first_type.append(i),
        1 : sec_type.append(i),
        2 : third_type.append(i),
        3 : four_type.append(i),
        4 : five_type.append(i),
        5 : six_type.append(i),
    }
    print(i)
    switch[index]
for i in range(6):
    writer.writerow([f'Cluster{i}'])
    a= []
    for ind in order_centroids[i,]:  # replace 6 with n words per cluster

        a.append(vocab_frame.loc[terms[ind].split(' ')].values.tolist()[0][0])
    writer.writerow(a)
    switch = {
        0: first_type,
        1: sec_type,
        2: third_type,
        3: four_type,
        4: five_type,
        5: six_type,
    }
    for item in switch[i]:
        writer.writerow([item[0].encode("utf-8").decode("latin1"),item[1],item[2]  ])
    writer.writerow([])
    writer.writerow([])
# for i in range(6):
#     print("Cluster %d words:" % i, end='')
#
#     writer.writerow([f'Cluster{i}'])
#     word_list = []
#     for ind in order_centroids[i, :]:  # replace 6 with n words per cluster
#         word_list.append(vocab_frame.loc[terms[ind].split(' ')].values.tolist()[0][0])
#     writer.writerow([' '.join(word_list)])
#     print()  # add whitespace
#     print()  # add whitespace
#
#     print("Cluster %d titles:" % i, end='')
#     print()
#     for title in loop_df[loop_df['cluster'] == i]['title'].tolist():
#         try:
#             print(' - %s' % title)
#             index = loop_df.title[loop_df.title == title].index.tolist()[0]
#
#             print('1', index)
#             # _ = loop_df.iloc[[index]].values
#             print(all_data[index])
#             print('2')
#             # _a = _[0][:3]
#             # print('3')
#             # writer.writerow([title.encode("utf-8").decode("latin1"), _a[1], _a[2]])
#             print("---")
#         except Exception as e:
#             print()
#             continue
#     print()  # add whitespace
#     print()  # add whitespacepace