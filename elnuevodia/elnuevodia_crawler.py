import requests
import re
from selenium import webdriver # 从selenium导入webdriver

from dateutil.parser import parse as time_parse
import csv
import time

class MSNBCCrawler():
    def __init__(self):
        self.search_page = "https://search.elnuevodia.com/Solr?q=hurricane%20Maria&facet=true&wt=json&_=1589985769614&start={}"
        self.result = []
        self.max_page = None
        self.url_and_title = []
        options = webdriver.ChromeOptions()
        # 设置中文
        options.add_argument('lang=zh_CN.UTF-8')
        options.add_argument("start-maximized")
        options.add_argument("--user-data-dir=selenium")
        options.add_argument("disable-infobars")
        options.add_argument("--disable-extensions")
        # 更换头部
        options.add_experimental_option("prefs", {'profile.managed_default_content_settings.javascript': 2})

        options.add_argument(
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36')
        self.driver = webdriver.Chrome("../msnbc/chromedriver",
                                       chrome_options=options)  # Optional argument, if not specified will search path.


    def get_search_result_page(self):
        # for page in range(0,10,15):
        for page in range(0,1110,15):
            if page == 0:
                page = 1
            print(page)
            json_res = requests.get(self.search_page.format(page))
            _ = json_res.json()
            for i in _['response']['docs']:
                date_posted = i['modDate_tdtd']
                url = i['url_s']
                title = i['titleWeb_texto']
                self.url_and_title.append({'date_posted': time_parse(date_posted), 'url': url, 'title': title})

    def get_content(self):
        for i in self.url_and_title:
            title = i["title"]
            url = i["url"]
            if 'https://video.foxnews.com/v/' in url:
                pass
            self.driver.get(url)

            time.sleep(3)
            for _ in range(0, 5):
                _content =  self.driver.find_elements_by_css_selector('.content-block p')

                i['content'] = ''
                for k in _content:
                    if len(k.text) > 0:
                        i['content'] += k.text
                if i['content'] == "":
                    print(url)
                    time.sleep(2)
                else:
                    break


    def insert_to_csv(self):
                col = ['title', 'post_dated', 'url', 'content']

                csvFile = open("elnuevodia.csv", "w")
                writer = csv.writer(csvFile)
                writer.writerow(col)
                for i in self.url_and_title:
                    if 'content' in i.keys():
                        # if 'puerto rico' in i['content'].lower() and 'hurricane maria' in i['content'].lower():
                        i['date_posted'] = str(i['date_posted']).split(' ')[0]
                        i['content'] = i['content'].replace('\n', '').strip()
                        writer.writerow([i['title'], i['date_posted'], i['url'], i['content']])

if __name__ == '__main__':
    instance = MSNBCCrawler()
    instance.get_search_result_page()
    instance.get_content()
    instance.insert_to_csv()

    #
    # r = requests.get("http://www.msnbc.com/katy-tur/watch/hud-inspector-gen-investigating-if-wh-slowed-puerto-rico-aid-1466076227973")
    #
    # condition_of_video_parse = re.compile('bcnews.com/j/MSNBC/Components/Video/', flags=re.DOTALL)
    # # print(condition_of_video_parse)
    # condition_of_video_parse = re.findall(condition_of_video_parse, r.text)
    # if condition_of_video_parse:
    #     content_parse = re.compile('property="og:description" content="(.*?)"/>', flags=re.DOTALL)
    #     content = re.findall(content_parse, r.text)
    # else:
    #     r = requests.get(url)
    #     parse = re.compile('rticleBody">(.*?) </div>', flags=re.DOTALL)
    #     # print(r.text.replace("\n",""))
    #     find_page_result = re.findall(parse, r.text.replace("\n", ""))[0]
    #     filter_content = re.sub(r'<a.*?">', "", find_page_result).replace("</a>", "").replace("<p>", "").replace(
    #         "</p>", "")
    #     print(filter_content)
    # print()
    #
    # # print(r.text.replace("\n",""))
