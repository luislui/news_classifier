import requests
import re
import csv
from selenium import webdriver # 从selenium导入webdriver

from dateutil.parser import parse as time_parse
from selenium import webdriver # 从selenium导入webdriver

from dateutil.parser import parse as time_parse

class MSNBCCrawler():
    def __init__(self):
        self.search_page = "http://www.msnbc.com/search/hurricane%20Maria"
        self.result = []
        self.max_page = None
        self.url_and_title = []


    def get_search_result_page(self):
        options = webdriver.ChromeOptions()
        # 设置中文

        options.add_experimental_option("prefs", {'profile.managed_default_content_settings.javascript': 1})

        options.add_argument("user-data-dir=selenium")
        # options.add_argument("user-data-dir=/Users/luilui/Library/ApplicationSupport/Google/Chrome/Defaul")
        # 更换头部
        options.add_argument(
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36')
        driver = webdriver.Chrome("../msnbc/chromedriver",
                                  chrome_options=options)  # Optional argument, if not specified will search path.
        base_url = "https://www.nytimes.com/search?query=hurricane+Maria"
        driver.get(base_url)
        import time
        show_more_xpath = "/html/body/div[1]/div[2]/main/div/div[2]/div[2]/div/button"
        for i in range(0,120):
            try:
                _ = driver.find_element_by_xpath(show_more_xpath)
                _.click()
                time.sleep(2)
            except:
                time.sleep(2)
        search_list = driver.find_elements_by_css_selector('.css-1l4w6pd')
        for i in search_list:

            url = i.find_element_by_css_selector('.css-e1lvw9 a').get_attribute('href') #css-myxawk
            title = i.find_element_by_css_selector('.css-2fgx4k').text #css-myxawk
            self.url_and_title.append({
                                      "url":url,
                                       "title":title}
                                        )
        driver.close()
        time.sleep(10)
    def get_content(self):
            options = webdriver.ChromeOptions()
            # 设置中文

            options.add_experimental_option("prefs", {'profile.managed_default_content_settings.javascript': 2})

            options.add_argument("user-data-dir=selenium")
            # options.add_argument("user-data-dir=/Users/luilui/Library/ApplicationSupport/Google/Chrome/Defaul")
            # 更换头部
            options.add_argument(
                'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36')
            driver = webdriver.Chrome("../msnbc/chromedriver",
                                      chrome_options=options)  # Optional argument, if not specified will search path.

            for index,i in enumerate(self.url_and_title):
                news_url = i["url"]
                driver.get(news_url)
                for g in range(0,10):
                    try:
                        import time
                        news_content = driver.find_elements_by_css_selector('.meteredContent p')

                        date_time_parse = re.compile('datetime="(.*?)"', re.S)
                        time_result = re.findall(date_time_parse, driver.page_source)
                        if len(time_result) > 0:
                            time_ = time_parse(time_result[0].split('T')[0])
                        else:
                            time_result = driver.find_elements_by_css_selector('.css-19m31ns')
                            time_ = time_parse(time_result[0].text.split('•')[-1])
                        i["content"] = ""
                        for item in news_content:
                            i["content"] += item.text
                        if not i["content"]:
                            #
                            i["content"] += driver.find_element_by_css_selector('.css-13qem32').text

                        i["date_posted"] = time_
                        print(i['url'])
                        print(i['content'])
                        break
                    except Exception as e:
                        print(i['url'])
                        print(e)
                        print("error")
                        time.sleep(1)

    def insert_to_csv(self):
                col = ['title', 'post_dated', 'url', 'content']

                csvFile = open("nytime.csv", "w")
                writer = csv.writer(csvFile)
                writer.writerow(col)
                for i in self.url_and_title:
                    if 'content' in i.keys() and 'date_posted' in i.keys():
                        # if 'puerto rico' in i['content'].lower() and 'hurricane maria' in i['content'].lower():
                        i['date_posted'] = str(i['date_posted']).split(' ')[0]
                        i['content'] = i['content'].replace('\n', '').strip()
                        writer.writerow([i['title'], i['date_posted'], i['url'], i['content']])
                    else:
                        writer.writerow([i['title'],"",i['url'],""])


if __name__ == '__main__':
    instance = MSNBCCrawler()
    instance.get_search_result_page()
    instance.get_content()
    instance.insert_to_csv()
    #
    # r = requests.get("http://www.msnbc.com/katy-tur/watch/hud-inspector-gen-investigating-if-wh-slowed-puerto-rico-aid-1466076227973")
    #
    # condition_of_video_parse = re.compile('bcnews.com/j/MSNBC/Components/Video/', flags=re.DOTALL)
    # # print(condition_of_video_parse)
    # condition_of_video_parse = re.findall(condition_of_video_parse, r.text)
    # if condition_of_video_parse:
    #     content_parse = re.compile('property="og:description" content="(.*?)"/>', flags=re.DOTALL)
    #     content = re.findall(content_parse, r.text)
    # else:
    #     r = requests.get(url)
    #     parse = re.compile('rticleBody">(.*?) </div>', flags=re.DOTALL)
    #     # print(r.text.replace("\n",""))
    #     find_page_result = re.findall(parse, r.text.replace("\n", ""))[0]
    #     filter_content = re.sub(r'<a.*?">', "", find_page_result).replace("</a>", "").replace("<p>", "").replace(
    #         "</p>", "")
    #     print(filter_content)
    # print()
    #
    # # print(r.text.replace("\n",""))
