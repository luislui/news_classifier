import csv
import sys
import re

csv.field_size_limit(92233720)
csvFile = open("npr.csv", "r",encoding="iso-8859-1")
processed_csvFile = open("processed_npr.csv", "w", encoding='iso-8859-1', newline ='')
reader = csv.reader(csvFile)
writer = csv.writer(processed_csvFile)
pattern = re.compile("^[a-zA-Z]+$")
for index , item in enumerate(reader):
    if index == 1:
        writer.writerow(item)
        continue
    if item[1] and not pattern.match(item[1]):
        writer.writerow(item)
